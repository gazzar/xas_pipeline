#!/usr/bin/env python

# Copyright (c) 2013 Synchrotron Light Source Australia Pty Ltd.
# Released under the Modified BSD license
# See LICENSE

import numpy as np
import xmap_netcdf_reader
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import Grid


def plot_grid(detector_data, pixel_step, grid, buffer_ix=0, show=False):
    """Plots a grid of spectra of the same shape as the array detector

    Keyword arguments:
    detector_data -
    pixel_step -
    grid - a matplotlib.axes_grid1 Grid object
    buffer_ix - 0-based int referring to buffer contained in netCDF file
    show - Boolean. Set True to call matplotlib show()

    """
    elements = detector_data.rows * detector_data.cols
    for detector_px in range(elements):
        module_ix, channel = divmod(detector_px, xmap_netcdf_reader.CHANNELS_PER_MODULE)
        try:
            row, col = divmod(detector_px, elements)
            spectrum = detector_data.spectrum(pixel_step, row, col)
        except:
            print 'pixel_step {} not found'.format(pixel_step)
            spectrum = np.zeros(detector_data.mca_bins, dtype=int)
        ax = grid[detector_px]
        ax.axis('off')
        ax.plot(spectrum)
    if show:
        plt.show()


def plot_step(detector_data, pixel_step, show=False):
    """Plot netCDF file contents via plot_grid

    Keyword arguments:
    detector_data -
    pixel_step -
    show - Boolean. Set True to call matplotlib show()

    """
    fig = plt.figure(figsize=(12,8))
    grid = Grid(fig, rect=111, nrows_ncols=(detector_data.rows, detector_data.cols),
            axes_pad=0.01, label_mode='L')

    plot_grid(detector_data, pixel_step, grid, show=show)
