#!/usr/bin/env python

# Copyright (c) 2013 Synchrotron Light Source Australia Pty Ltd.
# Released under the Modified BSD license
# See LICENSE

from netCDF_plot import plot_step
from xmap_netcdf_reader import DetectorData


FILE_DIR = r'tests/testdata/one_ioc'

if __name__ == "__main__":
    detector_data = DetectorData(shape=(10,10), pixelsteps_per_buffer=1, buffers_per_file=1,
                                 dirpaths=FILE_DIR, filepattern='Cdstandard94test1_([0-9]*)',
                                 mca_bins=2048, first_file_n=1)
    pixel_step = 0
    plot_step(detector_data, pixel_step, show=True)
