#!/usr/bin/env python

# Copyright (c) 2013 Synchrotron Light Source Australia Pty Ltd.
# Released under the Modified BSD license
# See LICENSE

from netCDF_plot import plot_step
from xmap_netcdf_reader import DetectorData
import os


BASE_DIR = r'tests/testdata/two_iocs/'
FILE1_DIR = os.path.join(BASE_DIR, r'ele100_1/out_1366616251')
FILE2_DIR = os.path.join(BASE_DIR, r'ele100_2/out_1366616251')

if __name__ == "__main__":
    detector_data = DetectorData(shape=(10,10), pixelsteps_per_buffer=4, buffers_per_file=5,
                                 dirpaths=(FILE1_DIR, FILE2_DIR), filepattern='ioc5[3-4]_([0-9]*)\.nc',
                                 mca_bins=2048, first_file_n=1)
    pixel_step = 9
    plot_step(detector_data, pixel_step, show=True)
