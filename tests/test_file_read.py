#!/usr/bin/env python

# Copyright (c) 2013 Synchrotron Light Source Australia Pty Ltd.
# Released under the Modified BSD license
# See LICENSE

import sys, os
PATH_HERE = os.path.abspath(os.path.dirname(__file__))
sys.path = [os.path.join(PATH_HERE, '..')] + sys.path
import unittest
import numpy as np
from scipy.io import netcdf_file
import xmap_netcdf_reader


TESTDATA_DIR = os.path.join(PATH_HERE, 'testdata')
ROWS = COLS = 10


class CorrectLoadIocTests(object):
    def setUp(self):
        pass

    def test_simple_load(self):
        self.assertTrue(isinstance(self.detector_data, xmap_netcdf_reader.DetectorData))

    def test_array_shape(self):
        self.assertEqual(self.detector_data.rows, ROWS)
        self.assertEqual(self.detector_data.cols, COLS)
        spectrum = self.detector_data.spectrum(pixel_step=0, row=0, col=0)
        self.assertEqual(spectrum.shape, (2048,))
        spectrum = self.detector_data.spectrum(pixel_step=0, row=0, col=0)    # read it again

    def test_element_indexing(self):
        spectrum = self.detector_data.spectrum(pixel_step=0, row=0, col=0)
        spectrum = self.detector_data.spectrum(pixel_step=0, row=0, col=COLS-1)
        spectrum = self.detector_data.spectrum(pixel_step=0, row=ROWS-1, col=0)
        spectrum = self.detector_data.spectrum(pixel_step=0, row=ROWS-1, col=COLS-1)

    def test_element_outside_range(self):
        with self.assertRaises(IndexError) as cm:
            spectrum = self.detector_data.spectrum(pixel_step=0, row=ROWS, col=COLS-1)
        with self.assertRaises(IndexError) as cm:
            spectrum = self.detector_data.spectrum(pixel_step=0, row=ROWS-1, col=COLS)


class CorrectLoadOneIocTests(unittest.TestCase, CorrectLoadIocTests):
    def setUp(self):
        FILE_DIR = os.path.join(TESTDATA_DIR, 'one_ioc')

        self.detector_data = xmap_netcdf_reader.DetectorData(
            shape=(ROWS,COLS), pixelsteps_per_buffer=1, buffers_per_file=1,
            dirpaths=FILE_DIR, filepattern='Cdstandard94test1_([0-9]*)',
            mca_bins=2048, first_file_n=1)

    def test_pixel_outside_range(self):
        spectrum = self.detector_data.spectrum(pixel_step=1, row=0, col=0)
        with self.assertRaises(IndexError) as cm:
            spectrum = self.detector_data.spectrum(pixel_step=2, row=0, col=0)


class CorrectLoadTwoIocsTests(unittest.TestCase, CorrectLoadIocTests):
    def setUp(self):
        BASE_DIR = os.path.join(TESTDATA_DIR, 'two_iocs')
        FILE1_DIR = os.path.join(BASE_DIR, r'ele100_1\out_1366616251')
        FILE2_DIR = os.path.join(BASE_DIR, r'ele100_2\out_1366616251')

        self.detector_data = xmap_netcdf_reader.DetectorData(
            shape=(10, 10), pixelsteps_per_buffer=4, buffers_per_file=5,
            dirpaths=(FILE1_DIR, FILE2_DIR), filepattern='ioc5[3-4]_([0-9]*)\.nc',
            mca_bins=2048, first_file_n=1)

    def test_pixel_outside_range(self):
        spectrum = self.detector_data.spectrum(pixel_step=9, row=0, col=0)
        with self.assertRaises(IndexError) as cm:
            spectrum = self.detector_data.spectrum(pixel_step=10, row=0, col=0)


class GetMatchingNTests(unittest.TestCase):
    def setUp(self):
        self.FILE_DIR = os.path.join(TESTDATA_DIR, 'one_ioc')

        self.pattern = 'Cdstandard94test1_([0-9]*)'
        self.detector_data = xmap_netcdf_reader.DetectorData(
            shape=(10, 10), pixelsteps_per_buffer=4, buffers_per_file=5,
            dirpaths=self.FILE_DIR, filepattern=self.pattern,
            mca_bins=2048, first_file_n=1)

    def test_matching_n(self):
        files = os.listdir(self.FILE_DIR)
        self.assertEqual(self.detector_data._matching_n(files, self.pattern, 1),
                         ['Cdstandard94test1_1.nc'])
        self.assertEqual(self.detector_data._matching_n(files, self.pattern, 2),
                         ['Cdstandard94test1_2.nc'])


class CorrectFilePathsTests(unittest.TestCase):
    def setUp(self):
        BASE_DIR = os.path.join(TESTDATA_DIR, 'two_iocs')
        self.FILE1_DIR = os.path.join(BASE_DIR, r'ele100_1\out_1366616251')
        self.FILE2_DIR = os.path.join(BASE_DIR, r'ele100_2\out_1366616251')

        self.detector_data = xmap_netcdf_reader.DetectorData(
            shape=(10, 10), pixelsteps_per_buffer=4, buffers_per_file=5,
            dirpaths=(self.FILE1_DIR, self.FILE2_DIR), filepattern='ioc5[3-4]_([0-9]*)\.nc',
            mca_bins=2048, first_file_n=1)

    def test_file_paths(self):
        paths = self.detector_data._get_file_paths_for_pixel_step(pixel_step=1)
        direct_path1 = os.path.join(self.FILE1_DIR, 'ioc53_1.nc')
        direct_path2 = os.path.join(self.FILE2_DIR, 'ioc54_1.nc')
        self.assertEqual(paths, [direct_path1, direct_path2])


class GetSpectrumTests(unittest.TestCase):
    def setUp(self):
        self.FILE_DIR = os.path.join(TESTDATA_DIR, 'one_ioc')

        self.detector_data = xmap_netcdf_reader.DetectorData(
            shape=(10, 10), pixelsteps_per_buffer=4, buffers_per_file=5,
            dirpaths=self.FILE_DIR, filepattern='Cdstandard94test1_([0-9]*)',
            mca_bins=2048, first_file_n=1)

    def test_get_fixedbins_spectrum_lowlevel(self):
        f = netcdf_file(os.path.join(self.FILE_DIR, 'Cdstandard94test1_1.nc'), 'r')
        spectrum = self.detector_data._get_fixedbins_spectrum(f, 0, 0, 0)
        self.assertEqual(spectrum.shape, (2048,))

    def test_get_fixedbins_spectrum(self):
        spectrum = self.detector_data.spectrum(0, 0, 0)
        self.assertEqual(spectrum.shape, (2048,))


class GetStatisticsTests(unittest.TestCase):
    def setUp(self):
        self.FILE_DIR = os.path.join(TESTDATA_DIR, 'one_ioc')

        self.detector_data = xmap_netcdf_reader.DetectorData(
            shape=(10, 10), pixelsteps_per_buffer=4, buffers_per_file=5,
            dirpaths=self.FILE_DIR, filepattern='Cdstandard94test1_([0-9]*)',
            mca_bins=2048, first_file_n=1)

    def test_get_statistic(self):
        fastpeaks = self.detector_data.statistic(0, 0, 0, 'realtime')
        self.assertEqual(type(fastpeaks), np.uint32)


class GetBufferHeaderItemTests(unittest.TestCase):
    def setUp(self):
        self.FILE_DIR = os.path.join(TESTDATA_DIR, 'one_ioc')

        self.detector_data = xmap_netcdf_reader.DetectorData(
            shape=(10, 10), pixelsteps_per_buffer=4, buffers_per_file=5,
            dirpaths=self.FILE_DIR, filepattern='Cdstandard94test1_([0-9]*)',
            mca_bins=2048, first_file_n=1)

    def test_get_item(self):
        item = self.detector_data.buffer_header_item(0, 0, 0, 'mapping_mode')
        self.assertEqual(type(item), np.uint16)
        self.assertEqual(item, 1)
        item = self.detector_data.buffer_header_item(0, 0, 0, 'tag1')
        self.assertEqual(item, 0xAA55)


class GetHeaderMode1ItemTests(unittest.TestCase):
    def setUp(self):
        self.FILE_DIR = os.path.join(TESTDATA_DIR, 'one_ioc')

        self.detector_data = xmap_netcdf_reader.DetectorData(
            shape=(10, 10), pixelsteps_per_buffer=4, buffers_per_file=5,
            dirpaths=self.FILE_DIR, filepattern='Cdstandard94test1_([0-9]*)',
            mca_bins=2048, first_file_n=1)

    def test_get_item(self):
        item = self.detector_data.pixel_header_mode1_item(0, 0, 0, 'total_pixel_block_size')
        self.assertEqual(type(item), np.uint32)
        item = self.detector_data.pixel_header_mode1_item(0, 0, 0, 'tag1')
        self.assertEqual(item, 0xCC33)


if __name__ == '__main__':
    import nose2
    nose2.main()