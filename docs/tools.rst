.. _tools_root:

xmap_netcdf_reader.py
*********************

Scope
-----

This page describes the xmap_netcdf_reader.py module.
We needed a fast netCDF reader that handles data from the 100-element detector on the XAS beamline at the Australian Synchrotron. To achieve high readout speeds, two IOCs (IOC-53 and IOC-54) are used in double-buffering mode to read the detector and write the data locally as netCDF files. These are then accessed from IOC-51 via Samba-mounted directories.

.. figure:: images/physical.*
   :width: 300px
   :alt: Physical setup on XAS
   
   Figure 1. 100-element detector on the AS-XAS beamline.

This architecture results in data at each pixel step [#pixel]_ (energy step or monochromator/DCM position) being spread across 2 files. 

The netCDF files written by the XMAP drivers are NETCDF3_CLASSIC file format, data type 3. There are a few Python modules that can read this format. xmap_netcdf_reader tries to import the reader from scipy.io.netcdf_file and falls back to pupynere and then to netCDF4 modules if unsuccessful. scipy.io.netcdf_file is a version of pupynere and these both read netCDF at the same high speed. Both are faster than the netCDF4 module. One reason for this is that the netCDF4 module reorders the endianness by default (this may be disabled).

xmap_netcdf_reader uses numpy views to achieve high performance. It will index and read an MCA spectrum in under 1ms. Higher throughput can be achieved for certain access patterns by caching the indexing operation. A @memoize decorator on the DetectorData class's _get_file_paths_for_pixel_step() method achieves this using a simple caching scheme. This caching has been disabled for the moment in case xmap_netcdf_reader is run as a daemon as part of a processing pipeline, which would currently cause the cache to grow indefinitely.

The file structure is shown in the following figure (See [#XIA08]_).

.. figure:: images/netCDF_structure.*
   :width: 500px
   :alt: netCDF structure

   Figure 2. netCDF structure.

In this example, pixels-per-buffer has been set to 4, which is the original setting we used, but this is currently set to 1 in the current version running on the beamline. xmap_netcdf_reader will handle these variations. We currently support mapping mode 1: Full Spectrum Mapping.

Assumptions
-----------

The indexing functions within the xmap_netcdf_reader DetectorData class make assumptions about the element detector to XMAP module wiring. These assumptions could be removed by replacing the indexing calculations with a lookup table read from a configuration file.

All detector elements are assumed to be mapped in sequential order through fully-populated XMAP modules with the XMAP modules divided equally between the IOCs, with the last IOC being underpopulated if necessary. In the example shown, corresponding to the AS-XAS beamline, IOC-53 has 13 XMAP modules and IOC-54 has 12 XMAP modules; a 13/12 split of the 25 modules required for reading the 100 elements. Any other split (e.g. 14/11 or 12/13) would break the assumption. If three IOCs were used instead, the modules would need to be divided between the IOCs as 9/9/7.

netCDF files containing data to be merged may be written to any number of named, specified directories. To enable aggregation of files containing the data from corresponding Pixel steps, filenames must contain some identified part that is used for grouping corresponding files. e.g. Our IOCs write the files ioc53_1.nc and ioc54_1.nc, which contain corresponding Pixel step data. These are paired based on the sequence number (1 in this case). Subsequent files ioc53_2.nc and ioc54_2.nc etc. are similarly paired. Once grouped, the files are ordered using a conventional sort.

Usage
-----

To access netCDF contents, first create an instance of the DetectorData class, specifying the detector and IOC configuration, e.g.::

    from xmap_netcdf_reader import DetectorData

    detector_data = DetectorData(shape=(10,10), pixelsteps_per_buffer=1, buffers_per_file=1,
                                 dirpaths='path_to_netcdf_files', filepattern='netcdf_filename_([0-9]*)',
                                 mca_bins=2048, first_file_n=1)

This specifies a 10-by-10-element detector (shape), the netCDF file location(s) (dirpaths), netCDF file-name pattern as a regular expression (filepattern),
mca-bins-per-element (mca_bins), the number of the lowest file containing the first pixel step in the run (first_file_n), and the pixels-per-buffer and buffers-per-file settings that determine when the current netCDF file in the sequence is closed and the next ones are created. The filepattern here says that netcdf files will have the form netcdf_filename_0, netcdf_filename_1, ... netcdf_filename_9999, etc. The parenthesized part defines a "regular expression capture group" that is used to group multiple files written by multiple IOCs. In this example, since we only have one IOC, the capture group is unimportant since no grouping is done.

Once we have created a DetectorData object, we simply access the spectra, or other values via one of the following methods::

    # Set pixel_step 5, detector element (0, 99)
    pixel_step = 5; row = 0; col = 99

    # Read spectrum ndarray
    spectrum = detector_data.spectrum(pixel_step, row, col)
    
    # Read fastpeaks ('realtime') value
    # For available statistic names see [#XIA08] and the buffer_header_dtype
    # and pixel_header_mode1_static_fixedbins_dtype dtype-definitions in xmap_netcdf_reader.py
    fastpeaks = detector_data.statistic(pixel_step, row, col, 'realtime')

    # Read mapping_mode value from buffer header
    item = detector_data.buffer_header_item(pixel_step, row, col, 'mapping_mode')

    # Read total_pixel_block_size value from mapping mode 1 pixel header
    item = detector_data.pixel_header_mode1_item(pixel_step, row, col, 'total_pixel_block_size')

Usage Examples
--------------

A system with a 100-element (10 by 10) detector, 2048 bins per MCA, single IOC writing one Pixel-step per netCDF file to a single directory (Linux)::

    from xmap_netcdf_reader import DetectorData

    FILE_DIR = r'~/netcdf_path'

    detector_data = DetectorData(shape=(10,10), pixelsteps_per_buffer=1, buffers_per_file=1,
                                 dirpaths=FILE_DIR, filepattern='Filename1_([0-9]*)',
                                 mca_bins=2048, first_file_n=1)
    # Read pixel_step 5, detector element (0, 99)
    spectrum = detector_data.spectrum(pixel_step=5, row=0, col=99)

A system with a 100-element (10 by 10) detector, 2048 bins per MCA, two IOCs writing four Pixel-steps per netCDF file to two directories (Windows). This demonstrates how the regular expression capture group is used to specify the part of the filepattern used for grouping::

    from xmap_netcdf_reader import DetectorData
    import os

    BASE_DIR = r'C:\Users\gary\2013-04-22_mapping_mode_collected\mnt\win'
    FILE1_DIR = os.path.join(BASE_DIR, r'ele100_1\out_1366616251')
    FILE2_DIR = os.path.join(BASE_DIR, r'ele100_2\out_1366616251')

    detector_data = DetectorData(shape=(10,10), pixelsteps_per_buffer=4, buffers_per_file=5,
                                 dirpaths=(FILE1_DIR, FILE2_DIR), filepattern='ioc5[3-4]_([0-9]*)\.nc',
                                 mca_bins=2048, first_file_n=1)

    # Read pixel_step 5, detector element (0, 99)
    spectrum = detector_data.spectrum(pixel_step=5, row=0, col=99)


.. rubric:: Footnotes

.. [#pixel] In XIA nomenclature, a "Pixel" is an acquisition of data from all elements in the detector. We use the term element to refer to 2048-bin MCA detector elements and "Pixel steps" to refer to each energy step defined by a monochromator position within an experimental run. An experimental run might have about 600 steps.

.. [#XIA08] XMAP User Manual, XIA LLC [XIA10] Digital X-ray Processor User's Manual, XIA LLC.
