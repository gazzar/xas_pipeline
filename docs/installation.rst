.. _installation_root:

***************
Installation
***************

XAS Tools is written in `Python <http://python.org>`_ (2.7 at the time of writing) and has several module dependencies that require installation. It is developed for Linux but should work on all platforms:

.. _python_package_dependencies:

Python package dependencies
---------------------------------

XAS Tools depends on the following Python modules being installed in the Python environment

* `numpy <http://numpy.scipy.org/>`_
* `scipy <http://www.scipy.org/>`_

Testing:

* `nose`

Those wishing to build the documentation from source will also need `Sphinx <http://sphinx.pocoo.org/>`_.
For those familiar with installing Python packages, the dependencies can be found in the `central repository of Python modules <http://pypi.python.org/pypi>`_. Linux users can typically find the dependencies using their package manager (e.g. synaptic or yum).

.. _example1:

Example 1. Linux installation on CentOS/Redhat/Fedora
-----------------------------------------------------

.. _example2:

Example 2. Linux installation on Debian-based systems using synaptic
--------------------------------------------------------------------

Note: untested.

This description is for Ubuntu or Mint Linux.

#. First, verify that Python2.7 is running correctly.
   e.g. for Ubuntu, open a terminal.
   At the ``$`` prompt type ``python -c "import sys; print sys.version"``.
   Verify that a string displays identifying a 2.7 branch version of Python.
#. Using synaptic or ``apt-get install <package>`` install the following packages: ``python-numpy``, ``python-scipy``
#. Visit the `XAS Tools download page <http://www.synchrotron.org.au/xas_tools>`_ and follow the instructions to obtain the package.
