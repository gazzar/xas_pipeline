.. XAS Tools documentation master file, created by
   sphinx-quickstart on Tue May 28 10:59:25 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to XAS Tools' documentation!
====================================

Contents
--------

.. toctree::
   :maxdepth: 2

   tools
   installation
   about

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

----

.. image:: images/synch_logo_60px.png
   :target: http://www.synchrotron.org.au/
   :alt: Australian Synchrotron logo

.. image:: images/nectar_logo_60px.png
   :target: http://nectar.org.au/
   :alt: NeCTAR logo

.. image:: images/versi_logo_60px.png
   :target: http://www.versi.edu.au/
   :alt: VeRSI logo
