XAS Tools
=========================================================

Python helper modules and general helper scripts for processing data from the XIA XMAP-based multi-element detector at the Australian Synchrotron.

Features
--------
- BSD-License

Requirements
------------
`XAS Tools`_ are written in Python 2.7 and depend on numpy and one of scipy, pupynere, or netCDF4.

Version History
---------------
0.1     Initial ...